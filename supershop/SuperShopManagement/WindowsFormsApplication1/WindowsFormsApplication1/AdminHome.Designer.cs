﻿namespace WindowsFormsApplication1
{
    partial class AdminHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.deletesalesmaninfoBTN = new System.Windows.Forms.Button();
            this.updatesalesmaninfoBTN = new System.Windows.Forms.Button();
            this.addsalesmanBTN = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deleteitemBTN = new System.Windows.Forms.Button();
            this.updateitemBTN = new System.Windows.Forms.Button();
            this.addnewitemBTN = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkprofitBTN = new System.Windows.Forms.Button();
            this.logoutBTN = new System.Windows.Forms.Button();
            this.soldItemsBTN = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.deletesalesmaninfoBTN);
            this.panel1.Controls.Add(this.updatesalesmaninfoBTN);
            this.panel1.Controls.Add(this.addsalesmanBTN);
            this.panel1.Location = new System.Drawing.Point(23, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 141);
            this.panel1.TabIndex = 0;
            // 
            // deletesalesmaninfoBTN
            // 
            this.deletesalesmaninfoBTN.Location = new System.Drawing.Point(36, 93);
            this.deletesalesmaninfoBTN.Name = "deletesalesmaninfoBTN";
            this.deletesalesmaninfoBTN.Size = new System.Drawing.Size(130, 23);
            this.deletesalesmaninfoBTN.TabIndex = 2;
            this.deletesalesmaninfoBTN.Text = "Delete Salesman Info";
            this.deletesalesmaninfoBTN.UseVisualStyleBackColor = true;
            // 
            // updatesalesmaninfoBTN
            // 
            this.updatesalesmaninfoBTN.Location = new System.Drawing.Point(36, 64);
            this.updatesalesmaninfoBTN.Name = "updatesalesmaninfoBTN";
            this.updatesalesmaninfoBTN.Size = new System.Drawing.Size(130, 23);
            this.updatesalesmaninfoBTN.TabIndex = 1;
            this.updatesalesmaninfoBTN.Text = "Update Salesman Info";
            this.updatesalesmaninfoBTN.UseVisualStyleBackColor = true;
            this.updatesalesmaninfoBTN.Click += new System.EventHandler(this.updatesalesmaninfoBTN_Click);
            // 
            // addsalesmanBTN
            // 
            this.addsalesmanBTN.Location = new System.Drawing.Point(36, 35);
            this.addsalesmanBTN.Name = "addsalesmanBTN";
            this.addsalesmanBTN.Size = new System.Drawing.Size(130, 23);
            this.addsalesmanBTN.TabIndex = 0;
            this.addsalesmanBTN.Text = "Add New Salesman";
            this.addsalesmanBTN.UseVisualStyleBackColor = true;
            this.addsalesmanBTN.Click += new System.EventHandler(this.addsalesmanBTN_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.deleteitemBTN);
            this.panel2.Controls.Add(this.updateitemBTN);
            this.panel2.Controls.Add(this.addnewitemBTN);
            this.panel2.Location = new System.Drawing.Point(247, 73);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 141);
            this.panel2.TabIndex = 1;
            // 
            // deleteitemBTN
            // 
            this.deleteitemBTN.Location = new System.Drawing.Point(35, 88);
            this.deleteitemBTN.Name = "deleteitemBTN";
            this.deleteitemBTN.Size = new System.Drawing.Size(130, 23);
            this.deleteitemBTN.TabIndex = 5;
            this.deleteitemBTN.Text = "Delete Item";
            this.deleteitemBTN.UseVisualStyleBackColor = true;
            // 
            // updateitemBTN
            // 
            this.updateitemBTN.Location = new System.Drawing.Point(35, 59);
            this.updateitemBTN.Name = "updateitemBTN";
            this.updateitemBTN.Size = new System.Drawing.Size(130, 23);
            this.updateitemBTN.TabIndex = 4;
            this.updateitemBTN.Text = "Update Item";
            this.updateitemBTN.UseVisualStyleBackColor = true;
            // 
            // addnewitemBTN
            // 
            this.addnewitemBTN.Location = new System.Drawing.Point(35, 30);
            this.addnewitemBTN.Name = "addnewitemBTN";
            this.addnewitemBTN.Size = new System.Drawing.Size(130, 23);
            this.addnewitemBTN.TabIndex = 3;
            this.addnewitemBTN.Text = "Add New Item";
            this.addnewitemBTN.UseVisualStyleBackColor = true;
            this.addnewitemBTN.Click += new System.EventHandler(this.addnewitemBTN_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.soldItemsBTN);
            this.panel3.Controls.Add(this.checkprofitBTN);
            this.panel3.Location = new System.Drawing.Point(484, 73);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 141);
            this.panel3.TabIndex = 1;
            // 
            // checkprofitBTN
            // 
            this.checkprofitBTN.Location = new System.Drawing.Point(70, 59);
            this.checkprofitBTN.Name = "checkprofitBTN";
            this.checkprofitBTN.Size = new System.Drawing.Size(95, 23);
            this.checkprofitBTN.TabIndex = 0;
            this.checkprofitBTN.Text = "Check Profit";
            this.checkprofitBTN.UseVisualStyleBackColor = true;
            this.checkprofitBTN.Click += new System.EventHandler(this.checkprofitBTN_Click);
            // 
            // logoutBTN
            // 
            this.logoutBTN.Location = new System.Drawing.Point(309, 265);
            this.logoutBTN.Name = "logoutBTN";
            this.logoutBTN.Size = new System.Drawing.Size(75, 23);
            this.logoutBTN.TabIndex = 2;
            this.logoutBTN.Text = "Log Out";
            this.logoutBTN.UseVisualStyleBackColor = true;
            this.logoutBTN.Click += new System.EventHandler(this.logoutBTN_Click);
            // 
            // soldItemsBTN
            // 
            this.soldItemsBTN.Location = new System.Drawing.Point(70, 87);
            this.soldItemsBTN.Name = "soldItemsBTN";
            this.soldItemsBTN.Size = new System.Drawing.Size(95, 23);
            this.soldItemsBTN.TabIndex = 1;
            this.soldItemsBTN.Text = "See Sold Items";
            this.soldItemsBTN.UseVisualStyleBackColor = true;
            this.soldItemsBTN.Click += new System.EventHandler(this.soldItemsBTN_Click);
            // 
            // AdminHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 371);
            this.Controls.Add(this.logoutBTN);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "AdminHome";
            this.Text = "Admin Home";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button deletesalesmaninfoBTN;
        private System.Windows.Forms.Button updatesalesmaninfoBTN;
        private System.Windows.Forms.Button addsalesmanBTN;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button deleteitemBTN;
        private System.Windows.Forms.Button updateitemBTN;
        private System.Windows.Forms.Button addnewitemBTN;
        private System.Windows.Forms.Button checkprofitBTN;
        private System.Windows.Forms.Button logoutBTN;
        private System.Windows.Forms.Button soldItemsBTN;
    }
}