﻿namespace WindowsFormsApplication1
{
    partial class AddNewItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.addBTN = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sellingpriceTB = new System.Windows.Forms.TextBox();
            this.buyingpriceTB = new System.Windows.Forms.TextBox();
            this.quantityTB = new System.Windows.Forms.TextBox();
            this.productnameTB = new System.Windows.Forms.TextBox();
            this.productidCB = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.addBTN);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.sellingpriceTB);
            this.panel1.Controls.Add(this.buyingpriceTB);
            this.panel1.Controls.Add(this.quantityTB);
            this.panel1.Controls.Add(this.productnameTB);
            this.panel1.Controls.Add(this.productidCB);
            this.panel1.Location = new System.Drawing.Point(61, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 273);
            this.panel1.TabIndex = 0;
            // 
            // addBTN
            // 
            this.addBTN.Location = new System.Drawing.Point(325, 236);
            this.addBTN.Name = "addBTN";
            this.addBTN.Size = new System.Drawing.Size(75, 23);
            this.addBTN.TabIndex = 3;
            this.addBTN.Text = "ADD ITEM";
            this.addBTN.UseVisualStyleBackColor = true;
            this.addBTN.Click += new System.EventHandler(this.addBTN_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 200);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Selling Price";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(169, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Buying Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Quantity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Product Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(169, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Product ID";
            // 
            // sellingpriceTB
            // 
            this.sellingpriceTB.Location = new System.Drawing.Point(300, 200);
            this.sellingpriceTB.Name = "sellingpriceTB";
            this.sellingpriceTB.Size = new System.Drawing.Size(100, 20);
            this.sellingpriceTB.TabIndex = 1;
            // 
            // buyingpriceTB
            // 
            this.buyingpriceTB.Location = new System.Drawing.Point(300, 165);
            this.buyingpriceTB.Name = "buyingpriceTB";
            this.buyingpriceTB.Size = new System.Drawing.Size(100, 20);
            this.buyingpriceTB.TabIndex = 1;
            // 
            // quantityTB
            // 
            this.quantityTB.Location = new System.Drawing.Point(300, 130);
            this.quantityTB.Name = "quantityTB";
            this.quantityTB.Size = new System.Drawing.Size(100, 20);
            this.quantityTB.TabIndex = 1;
            // 
            // productnameTB
            // 
            this.productnameTB.Location = new System.Drawing.Point(300, 93);
            this.productnameTB.Name = "productnameTB";
            this.productnameTB.Size = new System.Drawing.Size(100, 20);
            this.productnameTB.TabIndex = 1;
            // 
            // productidCB
            // 
            this.productidCB.FormattingEnabled = true;
            this.productidCB.Location = new System.Drawing.Point(300, 52);
            this.productidCB.Name = "productidCB";
            this.productidCB.Size = new System.Drawing.Size(121, 21);
            this.productidCB.TabIndex = 0;
            // 
            // AddNewItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 425);
            this.Controls.Add(this.panel1);
            this.Name = "AddNewItem";
            this.Text = "AddNewItem";
            this.Load += new System.EventHandler(this.AddNewItem_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox sellingpriceTB;
        private System.Windows.Forms.TextBox buyingpriceTB;
        private System.Windows.Forms.TextBox quantityTB;
        private System.Windows.Forms.TextBox productnameTB;
        private System.Windows.Forms.ComboBox productidCB;
        private System.Windows.Forms.Button addBTN;
    }
}