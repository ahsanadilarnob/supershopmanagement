﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class SoldItems : Form
    {
        public SoldItems()
        {
            InitializeComponent();
        }

        private void checkBTN_Click(object sender, EventArgs e)
        {
            String startDate = startDateDTP.Text;
            String endDate = endDateDTP.Text;

            //DBConnect.cs initialization
            MySqlConnection con = DBConnect.Initialize();
            con.Open();

            MySqlDataAdapter DA = new MySqlDataAdapter("SELECT `sales_id`, `product_id`,`quantity`,`profit` FROM `sales_table` WHERE `date` BETWEEN '"+startDate+"' AND '"+endDate+"'", con);
            DataSet DS = new DataSet();
            DA.Fill(DS);
            dataGridView1.DataSource = DS.Tables[0];

        }

        private void SoldItems_Load(object sender, EventArgs e)
        {
            startDateDTP.CustomFormat = "yyy-MM-d";
            startDateDTP.Format = DateTimePickerFormat.Custom;
            endDateDTP.CustomFormat = "yyy-MM-d";
            endDateDTP.Format = DateTimePickerFormat.Custom;
        }

        private void backBTN_Click(object sender, EventArgs e)
        {
            AdminHome adminHomeObj = new AdminHome();
            adminHomeObj.Visible = true;
            this.Hide();
        }

        private void dailyBTN_Click(object sender, EventArgs e)
        {

            //Getting Current Date
            DateTime dateAndTime = DateTime.Now;
            String currentDate = dateAndTime.ToString("yyyy-MM-dd");

            //DBConnect.cs initialization
            MySqlConnection con = DBConnect.Initialize();
            con.Open();

            MySqlDataAdapter DA = new MySqlDataAdapter("SELECT `sales_id`, `product_id`,`quantity`,`profit` FROM `sales_table` WHERE `date` = '" + currentDate + "'", con);
            DataSet DS = new DataSet();
            DA.Fill(DS);
            dataGridView1.DataSource = DS.Tables[0];

        }
    }
}
