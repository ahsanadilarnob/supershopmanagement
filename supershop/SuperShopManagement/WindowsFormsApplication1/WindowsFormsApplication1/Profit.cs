﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Profit : Form
    {
        public Profit()
        {
            InitializeComponent();
        }

        private void Profit_Load(object sender, EventArgs e)
        {
            startDateDTP.CustomFormat = "yyy-MM-d";
            startDateDTP.Format = DateTimePickerFormat.Custom;
            endDateDTP.CustomFormat = "yyy-MM-d";
            endDateDTP.Format = DateTimePickerFormat.Custom;


        }
        private void checkBTN_Click(object sender, EventArgs e)
        {
            String startDate = startDateDTP.Text;
            String endDate = endDateDTP.Text;

            //DBConnect.cs initialization
            MySqlConnection con = DBConnect.Initialize();
            con.Open();

            //Getting profit from Database
            String profitSql = "";
            profitSql = "SELECT SUM(`profit`) FROM `sales_table` WHERE `date` BETWEEN '"+ startDate +"' AND '"+ endDate +"'";
            MySqlCommand cmd = new MySqlCommand(profitSql, con);
            String totalProfit = Convert.ToString(cmd.ExecuteScalar().ToString());

            MessageBox.Show(totalProfit);
            con.Close();
        }

        private void dailyBTN_Click(object sender, EventArgs e)
        {
            MySqlConnection con = DBConnect.Initialize();
            con.Open();
            
            //Getting Current Date
            DateTime dateAndTime = DateTime.Now;
            String currentDate = dateAndTime.ToString("yyyy-MM-dd");
            //MessageBox.Show(currentDate);
            //Getting profit for current date from Database
            String todaysProfitSql = "";
            todaysProfitSql = "SELECT SUM(`profit`) FROM `sales_table` WHERE `date` = '"+ currentDate +"'";
            MySqlCommand cmd = new MySqlCommand(todaysProfitSql, con);
            String todaysTotalProfit = Convert.ToString(cmd.ExecuteScalar().ToString());
            MessageBox.Show("Todays Profit = " + todaysTotalProfit);
            con.Close();
        }

        private void backBTN_Click(object sender, EventArgs e)
        {
            AdminHome adminHomeObj = new AdminHome();
            adminHomeObj.Visible = true;
            this.Hide();
        }
    }
}
