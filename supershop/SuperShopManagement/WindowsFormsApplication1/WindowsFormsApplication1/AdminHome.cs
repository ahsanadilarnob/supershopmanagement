﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AdminHome : Form
    {
        public AdminHome()
        {
            InitializeComponent();
        }

        private void logoutBTN_Click(object sender, EventArgs e)
        {
            LoginPage LoginPageObj = new LoginPage();
            LoginPageObj.Visible = true;
            this.Hide();
        }

        private void addsalesmanBTN_Click(object sender, EventArgs e)
        {
            AddNewSalesman AddNewSalesManObj = new AddNewSalesman();
            AddNewSalesManObj.Visible = true;
            this.Hide();
        }

        private void addnewitemBTN_Click(object sender, EventArgs e)
        {
            AddNewItem AddNewItemObj = new AddNewItem();
            AddNewItemObj.Visible = true;
            this.Hide();
        }

        private void updatesalesmaninfoBTN_Click(object sender, EventArgs e)
        { 
            UpdateSalesman UpdateSalesmanObj = new UpdateSalesman();
            UpdateSalesmanObj.Visible = true;
            this.Hide();
        }

        private void checkprofitBTN_Click(object sender, EventArgs e)
        {
            Profit profitObj = new Profit();
            profitObj.Visible = true;
            this.Hide();
        }

        private void soldItemsBTN_Click(object sender, EventArgs e)
        {
            SoldItems soldItemsObj = new SoldItems();
            soldItemsObj.Visible = true;
            this.Hide();
        }
    }
}
