﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    class DBConnect
    {
        MySqlConnection connection = Initialize();
        public static MySqlConnection Initialize()
        {
            MySqlConnection connection;
            String connection_str;

            String server = "localhost";
            String database = "super_shop_management_db";
            String uid = "root";
            String password = "root";

            connection_str = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connection_str);
            return connection;
        }
    }
}
