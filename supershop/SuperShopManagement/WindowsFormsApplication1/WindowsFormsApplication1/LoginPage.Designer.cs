﻿namespace WindowsFormsApplication1
{
    partial class LoginPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.loginBTN = new System.Windows.Forms.Button();
            this.adminRB = new System.Windows.Forms.RadioButton();
            this.salesRB = new System.Windows.Forms.RadioButton();
            this.passwordTB = new System.Windows.Forms.TextBox();
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.loginGB = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.loginGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.loginBTN);
            this.panel1.Controls.Add(this.adminRB);
            this.panel1.Controls.Add(this.salesRB);
            this.panel1.Controls.Add(this.passwordTB);
            this.panel1.Controls.Add(this.usernameTB);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(119, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(324, 219);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Username";
            // 
            // loginBTN
            // 
            this.loginBTN.Font = new System.Drawing.Font("Mimmo", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginBTN.ForeColor = System.Drawing.Color.LawnGreen;
            this.loginBTN.Location = new System.Drawing.Point(116, 161);
            this.loginBTN.Name = "loginBTN";
            this.loginBTN.Size = new System.Drawing.Size(102, 41);
            this.loginBTN.TabIndex = 4;
            this.loginBTN.Text = "LOGIN";
            this.loginBTN.UseVisualStyleBackColor = true;
            this.loginBTN.Click += new System.EventHandler(this.loginBTN_Click);
            // 
            // adminRB
            // 
            this.adminRB.AutoSize = true;
            this.adminRB.Location = new System.Drawing.Point(120, 122);
            this.adminRB.Name = "adminRB";
            this.adminRB.Size = new System.Drawing.Size(72, 24);
            this.adminRB.TabIndex = 3;
            this.adminRB.TabStop = true;
            this.adminRB.Text = "Admin";
            this.adminRB.UseVisualStyleBackColor = true;
            // 
            // salesRB
            // 
            this.salesRB.AutoSize = true;
            this.salesRB.Location = new System.Drawing.Point(120, 99);
            this.salesRB.Name = "salesRB";
            this.salesRB.Size = new System.Drawing.Size(98, 24);
            this.salesRB.TabIndex = 2;
            this.salesRB.TabStop = true;
            this.salesRB.Text = "Salesman";
            this.salesRB.UseVisualStyleBackColor = true;
            // 
            // passwordTB
            // 
            this.passwordTB.Location = new System.Drawing.Point(120, 55);
            this.passwordTB.Name = "passwordTB";
            this.passwordTB.PasswordChar = '*';
            this.passwordTB.Size = new System.Drawing.Size(162, 26);
            this.passwordTB.TabIndex = 1;
            // 
            // usernameTB
            // 
            this.usernameTB.Location = new System.Drawing.Point(120, 28);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.Size = new System.Drawing.Size(162, 26);
            this.usernameTB.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Location = new System.Drawing.Point(21, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(683, 55);
            this.label3.TabIndex = 1;
            this.label3.Text = "SUPERSHOP MANAGEMENT";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(0, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            // 
            // loginGB
            // 
            this.loginGB.Controls.Add(this.panel1);
            this.loginGB.Font = new System.Drawing.Font("Mimmo", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loginGB.ForeColor = System.Drawing.Color.Maroon;
            this.loginGB.Location = new System.Drawing.Point(102, 77);
            this.loginGB.Name = "loginGB";
            this.loginGB.Size = new System.Drawing.Size(541, 277);
            this.loginGB.TabIndex = 2;
            this.loginGB.TabStop = false;
            this.loginGB.Text = "Login";
            // 
            // LoginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(729, 380);
            this.Controls.Add(this.loginGB);
            this.Controls.Add(this.label3);
            this.Name = "LoginPage";
            this.Text = "Login Page";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.loginGB.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button loginBTN;
        private System.Windows.Forms.RadioButton adminRB;
        private System.Windows.Forms.RadioButton salesRB;
        private System.Windows.Forms.TextBox passwordTB;
        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox loginGB;
    }
}

