﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class AddNewItem : Form
    {
        public AddNewItem()
        {
            InitializeComponent();
        }

        private void addBTN_Click(object sender, EventArgs e)
        {
            
        }

        private void AddNewItem_Load(object sender, EventArgs e)
        {
            MySqlConnection con = DBConnect.Initialize();
            con.Open();
            String sql = "";
            sql = "Select `product_id` from `product_table`";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            MySqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                productidCB.Items.Add(DR[0]);

            }
            con.Close();

        }
    }
}
