﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Drawing.Printing;
using Microsoft.VisualBasic;
using System.Threading.Tasks;



namespace WindowsFormsApplication1
{
    public partial class Sales : Form
    {
        
        public Sales()
        {
            InitializeComponent();
        }

        //Initializing Textboxes To NULL values after the user clicks on ADD MORE button
        public void ini()
        {
            productIDTB.Text = "";
            quantityTB.Text = "";
        }
        
        private void addBTN_Click(object sender, EventArgs e)
        {
            //Getting the values from TextBoxes
            String productID = productIDTB.Text;
            String quantity = quantityTB.Text;

            //Initializing DBConnet.cs and opening Connection
            MySqlConnection con = DBConnect.Initialize();
            con.Open();
            
            //Querying Product Price from database
            String priceSql = "";
            priceSql = "SELECT `sellingPrice` FROM `product_table` WHERE `product_id` = '" + productID + "'";
            MySqlCommand cmd = new MySqlCommand(priceSql, con);

            //Executing MySql Command and Converting price to Integer 
            int price = Convert.ToInt32(cmd.ExecuteScalar().ToString());

            //Querying Name from database
            String nameSql = "";
            nameSql = "SELECT `name` FROM `product_table` WHERE `product_id` = '" + productID + "'";
            MySqlCommand cmdName = new MySqlCommand(nameSql, con);

            //Executing MySql Command and Converting Name to String 
            String name = Convert.ToString(cmdName.ExecuteScalar().ToString());
            
            //Converting Quantity which was taken from user from Quantity TextBox
            int quantityInt = Convert.ToInt32(quantity);

            //Calculating Product Price
            price = price * quantityInt;

            //Adding Rows To DataGriedView
            dataGridView1.Rows.Add(productIDTB.Text,name,quantityTB.Text, price);

            //Clearing the texbox after getting value
            ini();

            //Closing the Connection to Database
            con.Close();
            
        }


        private void Sales_Load(object sender, EventArgs e)
        {
            //This is for dynamically loading product ID from database to suggest while typing in the textbox
            //Referred to AutoCompleteTB.cs
            AutoCompleteTB autoCompleteTBObj = new AutoCompleteTB();
            autoCompleteTBObj.autoComplete(productIDTB,"product_id","product_table");
            
        }

        public void saveBTN_Click(object sender, EventArgs e)
        {
            int numberOfRows = dataGridView1.Rows.Count;
            int totalPrice = 0;
            int priceValue = 0;
            int profit = 0;
            int singleProfit = 0;
            int dataGriedViewQuantity = 0;
            String dataGriedViewProductID = "";
            String insertSql = "";
            String profitSql = "";
            String decreaseQuantitySql = "";

            //Generating A Unique Sales ID for the current sale
            SalesIDGenerator generateSalesIDObj = new SalesIDGenerator();
            String salesID = generateSalesIDObj.GenerateSalesID();
            MessageBox.Show(salesID);
            

            //Getting Current Date
            DateTime dateAndTime = DateTime.Now;
            String date = dateAndTime.ToString("yyyy-MM-dd");

            //Initializing DBConnect.cs
            MySqlConnection con = DBConnect.Initialize();
            con.Open();
            
            String profitSelect = "";

            for (int i = 0; i < numberOfRows - 1; i++)
            {
                //Getting ProductID and Quantity From DataGriedView Row Fields
                dataGriedViewProductID = dataGridView1.Rows[i].Cells["ProductID"].Value.ToString();
                dataGriedViewQuantity = int.Parse(dataGridView1.Rows[i].Cells["Quantity"].Value.ToString());
                

                //SQL Query for retrivieng profit from Databse
                profitSelect = "SELECT `profit` FROM `product_table` WHERE `product_id` = '" + dataGriedViewProductID + "'";
                MySqlCommand cmdProfit = new MySqlCommand(profitSelect,con);

                //Calculating Profit for a single Product from GriedView
                profit = Convert.ToInt32(cmdProfit.ExecuteScalar().ToString());
                singleProfit = dataGriedViewQuantity * profit;

                //Inserting sales data into sales_table
                insertSql = "INSERT INTO `sales_table` (`id`, `sales_id`, `product_id`, `quantity`, `profit`, `date`) VALUES (NULL, '" + salesID + "' , '" + dataGriedViewProductID + "', '" + dataGriedViewQuantity + "', '" + singleProfit + "', '" + date + "');";
                MySqlCommand cmd = new MySqlCommand(insertSql, con);
                cmd.ExecuteNonQuery();

                //Decreasing sold out product quantity
                decreaseQuantitySql = "UPDATE `product_table` SET `quantity` = `quantity`-'" + dataGriedViewQuantity + "' WHERE `product_id` = '" + dataGriedViewProductID + "'; ";
                MySqlCommand cmdDecrease = new MySqlCommand(decreaseQuantitySql, con);
                cmdDecrease.ExecuteNonQuery();

                priceValue = int.Parse(dataGridView1.Rows[i].Cells["Price"].Value.ToString());


                //calculating total price for customer invoice
                totalPrice = totalPrice + priceValue;
            }

            //Printing


            printDocument1.Print();
            //MessageBox.Show(totalPrice.ToString());

           
            dataGridView1.Rows.Clear();






        }

        public void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int numberOfRows = dataGridView1.Rows.Count;
            int dataGriedViewQuantity = 0;
            String dataGriedViewProductName = "";
            int priceValue = 0;
            String dataGriedViewProductID = "";
            int totalPrice = 0;

            //Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            Graphics graphic = e.Graphics;
            int startX = -10;
            int startY = 50;
            int offset = 40;
            int offx = 150;
            Font font = new Font("Courier New", 12);
            //int x = 65;
            // int y = -20;
            //int x2 = 5 + 2;
            // int y2 = 5 + 2;
            String line = "_________________________________________________________________";


            //Getting Current Date
            DateTime dateAndTime = DateTime.Now;
            String date = dateAndTime.ToString("yyyy-MM-dd");


            graphic.DrawString("RMAAS SUPER SHOP ", new Font("Courier New", 18), new SolidBrush(Color.Black), 200, -30);
            graphic.DrawString("Customer Invoice ", new Font("Courier New", 12), new SolidBrush(Color.Black), 250, 7);
            graphic.DrawString(date, new Font("Courier New", 12), new SolidBrush(Color.Black), 600, 7);
            graphic.DrawString(line, new Font("Courier New", 12), new SolidBrush(Color.Black), -10, 60);
            // dataGridView1.DrawToBitmap(bm, new Rectangle(0, 0, this.dataGridView1.Width, this.dataGridView1.Height));
            string top = "Item Name".PadRight(30) + "Quantity".PadRight(30) + "Price";
            graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY);
            

            for (int i = 0; i < numberOfRows-1; i++)
            {
                //Getting ProductID and Quantity From DataGriedView Row Fields
                dataGriedViewProductName = dataGridView1.Rows[i].Cells["productName"].Value.ToString();
                dataGriedViewQuantity = int.Parse(dataGridView1.Rows[i].Cells["Quantity"].Value.ToString());
                priceValue = int.Parse(dataGridView1.Rows[i].Cells["Price"].Value.ToString());
                dataGriedViewProductID = dataGridView1.Rows[i].Cells["ProductID"].Value.ToString();



                graphic.DrawString(dataGriedViewProductName, font, new SolidBrush(Color.Black), -10 , 50 + offset);
                graphic.DrawString(dataGriedViewQuantity.ToString(), font, new SolidBrush(Color.Black), 300 , 50 + offset);
                graphic.DrawString(priceValue.ToString(), font, new SolidBrush(Color.Black), 610, 50 + offset);
                offset = offset + 40;

                


                //calculating total price for customer invoice
                totalPrice = totalPrice + priceValue;
            }
            String line2 = "______";
            graphic.DrawString(line2, new Font("Courier New", 12), new SolidBrush(Color.Black), 600, 50 + offset);
            offset = offset + 40;
            graphic.DrawString(totalPrice.ToString(), font, new SolidBrush(Color.Black), 610, 50 + offset);
            graphic.DrawString("Total Price", font, new SolidBrush(Color.Black), 400, 50 + offset);
            offset = offset + 40;
            graphic.DrawString("THANK YOU", new Font("Courier New", 18), new SolidBrush(Color.Black), 250, 50 + offset);
            //e.Graphics.DrawImage(bm, 50, 50+offset);

        }

        private void logOutBTN_Click(object sender, EventArgs e)
        {
            LoginPage loginPageObj = new LoginPage();
            loginPageObj.Visible = true;
            this.Hide();
        }
    }
}
