﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class LoginPage : Form
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        public void ini()
        {
            usernameTB.Text = "";
            passwordTB.Text = "";
        }

        private void loginBTN_Click(object sender, EventArgs e)
        {
            //Initializing DBConnect.cs
            MySqlConnection con = DBConnect.Initialize();

            //Checking if the value is empty
            if (usernameTB.Text == "" || passwordTB.Text == "")
            {
                MessageBox.Show("Username or password cannot be null");
                return;
            }

            //Opening Connection
            con.Open();

            //Getting Values from the TextBoxes and Initializing a String for SQL query
            String sql = "";
            String username = usernameTB.Text;
            String password = passwordTB.Text;

            //If Admin RadioButton is Checked then this query will be executed later
            if (adminRB.Checked)
            {
                sql = "SELECT `admin_username`, `admin_password` FROM `admin_login` WHERE `admin_username`='" + username + "' and `admin_password`='" + password + "'";
            }

            //If Salesman RadioButton is Checked then this query will be executed later
            else if (salesRB.Checked)
            {
                sql = "SELECT `sales_username`, `sales_password` FROM `salesman_info` WHERE `sales_username`='" + username + "' and `sales_password`='" + password + "'";
            }
            else
            {
                MessageBox.Show("Please choose if you are admin or salesman!");
                return;
            }

            //Executing Query
            MySqlCommand cmd = new MySqlCommand(sql, con);

            //Reading the Rows
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                if (adminRB.Checked)
                {
                    ini();
                    //Go to Admin Home
                    AdminHome adminHomeObj = new AdminHome();
                    adminHomeObj.Visible = true;
                    this.Hide();
                }
                if (salesRB.Checked)
                {
                    ini();
                    //Go to Salesman Home
                    Sales salesObj = new Sales();
                    salesObj.Visible = true;
                    this.Hide();
                }

            }
            else
            {
                MessageBox.Show("Username or password does not match.");
                return;
            }

            //Closing reader
            reader.Close();

            //Closing Connection
            con.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Sales salesObj = new Sales();
            salesObj.Visible = true;
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            AdminHome adminHomeObj = new AdminHome();
            adminHomeObj.Visible = true;
        }
    }
}
