﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using WindowsFormsApplication1;

namespace WindowsFormsApplication1
{
    class AutoCompleteTB
    {
        MySqlConnection con = DBConnect.Initialize();
        MySqlDataReader reader;
        MySqlCommand cmd;

        public void autoComplete (TextBox autoCompleteTB, String columnName, String tableName)
        {
            String sql = "";
            sql = "SELECT `" + columnName + "` FROM `" + tableName + "`";
            con.Open();
            cmd = new MySqlCommand(sql,con);
            reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                autoCompleteTB.AutoCompleteCustomSource.Add(reader[columnName].ToString());
            }
            reader.Close();
            con.Close();
        }
    }
}
