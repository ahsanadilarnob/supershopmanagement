﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    class SalesIDGenerator
    {
        public string GenerateSalesID()
        {
            String salesID = DateTime.Now.ToString("ddMMyyHHmmss");
            return salesID;
        }
    }
}
