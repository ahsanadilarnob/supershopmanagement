﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class DeleteSalesman : Form
    {
        public DeleteSalesman()
        {
            InitializeComponent();
        }

        private void deleteBTN_Click(object sender, EventArgs e)
        {
            String name = nameTB.Text;
            String mobileNumber = mobilenumberTB.Text;
            String username = usernameTB.Text;
            String password = passwordTB.Text;

            MySqlConnection con = DBConnect.Initialize();
            con.Open();
            String sql = "";
            sql = "DELETE FROM `salesman_info` WHERE WHERE `sales_username`='" + username + "'";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Salesman Info Successfully Updated");
            ini();
            con.Close();
        }
        public void ini()
        {
            deleteBTN.Enabled = false;
            nameTB.Text = "";
            mobilenumberTB.Text = "";
            usernameTB.Text = "";
            passwordTB.Text = "";
        }

        private void searchBTN_Click(object sender, EventArgs e)
        {
            String search = searchTB.Text;
            MySqlConnection con = DBConnect.Initialize();

            if (search == "")
            {
                MessageBox.Show("Please enter a username to search!!");
                return;
            }

            con.Open();

            String sql = "";
            sql = "SELECT * FROM `salesman_info` WHERE `sales_username`='" + search + "'";
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader dataReader = cmd.ExecuteReader();
            if (dataReader.Read())
            {
                nameTB.Text = dataReader.GetValue(3).ToString();
                mobilenumberTB.Text = dataReader.GetValue(4).ToString();
                usernameTB.Text = dataReader.GetValue(1).ToString();
                passwordTB.Text = dataReader.GetValue(2).ToString();
                deleteBTN.Enabled = true;
            }
            else
            {
                MessageBox.Show("No Salesman found with the username " + search);
                return;
            }
        }

    }
}
